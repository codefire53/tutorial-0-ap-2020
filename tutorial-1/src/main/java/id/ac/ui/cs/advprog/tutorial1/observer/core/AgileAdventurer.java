package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                this.guild=guild;
                this.guild.add(this);
                //ToDo: Complete Me
        }

        //ToDo: Complete Me
        public void update(){
                Quest currQuest=this.guild.getQuest();
                if(!this.guild.getQuestType().equals("E")) {
                        this.getQuests().add(currQuest);
                }

        }
}
