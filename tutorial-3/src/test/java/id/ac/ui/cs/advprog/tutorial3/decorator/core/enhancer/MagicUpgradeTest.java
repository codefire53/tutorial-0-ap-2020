package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MagicUpgradeTest {


    private MagicUpgrade magicUpgrade;

    @BeforeEach
    public void setUp(){
        magicUpgrade = new MagicUpgrade(new Longbow());
    }

    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals("Longbow",magicUpgrade.getName());
    }

    @Test
    public void testGetMethodWeaponDescription(){
        //TODO: Complete me
        assertEquals("Magic Big Longbow",magicUpgrade.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        assertTrue( 30 <= magicUpgrade.getWeaponValue() && magicUpgrade.getWeaponValue() <= 35);
    }
}
