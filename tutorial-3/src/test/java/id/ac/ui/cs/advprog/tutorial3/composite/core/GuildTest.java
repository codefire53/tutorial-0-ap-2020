package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
        assertEquals(guildMaster,guild.getMember("Eko","Master"));
        Member rin=new PremiumMember("Rin Tohsaka","Servant Master");
        Member archer=new OrdinaryMember("Shirou", "Archer");
        guild.addMember(guildMaster,rin);
        guild.addMember(rin,archer);
        assertEquals(rin,guild.getMember("Rin Tohsaka","Servant Master"));
        assertEquals(archer,guild.getMember("Shirou","Archer"));
        assertEquals(3,guild.getMemberList().size());
    }

    @Test
    public void testMethodAddMember() {
        //TODO: Complete me
        Member arthuria=new OrdinaryMember("Arthuria Pendragon","Saber");
        guild.addMember(guildMaster,arthuria);
        assertEquals(arthuria,guild.getMember("Arthuria Pendragon","Saber"));
    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        Member skadi=new OrdinaryMember("Skadi","Assassin");
        guild.addMember(guildMaster,skadi);
        guild.removeMember(guildMaster,skadi);
        assertTrue(guild.getMember("Skadi","Assassin")==null);
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }


}
