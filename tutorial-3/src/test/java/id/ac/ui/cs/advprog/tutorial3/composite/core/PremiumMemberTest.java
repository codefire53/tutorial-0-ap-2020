package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals("Wati",member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Gold Merchant",member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        int len=member.getChildMembers().size();
        member.addChildMember(new PremiumMember("Silverash","Guard"));
        assertTrue(member.getChildMembers().size() > len);
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member exu=new PremiumMember("Exusiai","Sniper");
        member.addChildMember(exu);
        int len=member.getChildMembers().size();
        member.removeChildMember(exu);
        assertTrue(member.getChildMembers().size() < len);
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member guildMaster=new PremiumMember("Megumin","Mage");
        Guild guild=new Guild(guildMaster);
        guild.addMember(guildMaster,member);
        guild.addMember(member,new OrdinaryMember("Funny Valentine","Stand User"));
        guild.addMember(member,new OrdinaryMember("Giorno Giovanna","Stand User"));
        guild.addMember(member,new OrdinaryMember("Joseph Joestar","Hamon User"));
        guild.addMember(member,new OrdinaryMember("Zeppeli","Hamon User"));
        assertTrue(member.getChildMembers().size() <= 3);
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Guild guild=new Guild(member);
        member.addChildMember(new OrdinaryMember("Pucci","Stand User"));
        member.addChildMember(new OrdinaryMember("Funny Valentine","Stand User"));
        member.addChildMember(new OrdinaryMember("Giorno Giovanna","Stand User"));
        member.addChildMember(new OrdinaryMember("Joseph Joestar","Hamon User"));
        assertTrue(member.getChildMembers().size() > 3);
    }
}
