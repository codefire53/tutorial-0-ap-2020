package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Longbow extends Weapon {
        //TODO: Complete me
        public Longbow(){
            this.weaponName="Longbow";
            this.weaponDescription="Big Longbow";
            this.weaponValue=15;
        }

    @Override
    public int getWeaponValue() {
        return this.weaponValue;
    }
    @Override
    public String getDescription(){
        return this.weaponDescription;
    }
    @Override
    public String getName(){
        return this.weaponName;
    }
}
