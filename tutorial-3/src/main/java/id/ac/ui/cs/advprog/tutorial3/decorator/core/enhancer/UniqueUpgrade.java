package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class UniqueUpgrade extends Weapon {

    Weapon weapon;

    public UniqueUpgrade(Weapon weapon){
        this.weapon = weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }


    // Senjata bisa dienhance hingga 10-15 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int buff=new Random().nextInt(6)+10;
        this.weaponValue=buff+this.weapon.getWeaponValue();
        return this.weaponValue;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        this.weaponDescription="Unique "+this.weapon.getDescription();
        return this.weaponDescription;
    }
}
