package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {

    Weapon weapon;

    public RegularUpgrade(Weapon weapon) {
        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int buff=new Random().nextInt(5)+1;
        this.weaponValue=buff+this.weapon.getWeaponValue();
        return this.weaponValue;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        this.weaponDescription="Regular "+this.weapon.getDescription();
        return this.weaponDescription;
    }
}
