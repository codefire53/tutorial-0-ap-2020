package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Gun extends Weapon {
        //TODO: Complete me
        public Gun(){
            this.weaponName="Gun";
            this.weaponDescription="Automatic Gun";
            this.weaponValue=20;
        }

    @Override
    public int getWeaponValue() {
            return this.weaponValue;
    }
    @Override
    public String getDescription(){
            return this.weaponDescription;
    }
    @Override
    public String getName(){
            return this.weaponName;
    }

}
