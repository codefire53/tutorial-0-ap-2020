package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
        //TODO: Complete me
        private String name;
        private String role;
        private List <Member> childMembers;
        public PremiumMember(String name,String role){
                this.name=name;
                this.role=role;
                this.childMembers=new ArrayList<Member>();
        }
        public String getName(){
                return this.name;
        }
        public String getRole(){
                return this.role;
        }
        @Override
        public void addChildMember(Member member) {
                this.childMembers.add(member);
        }
        @Override
        public void removeChildMember(Member member){
                this.childMembers.remove(member);
        }
        @Override
        public List <Member> getChildMembers(){
                return this.childMembers;
        }
        @Override
        public int getChildSize(){
                return this.childMembers.size();
        }


}
