package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Sword extends Weapon {
        //TODO: Complete me
        public Sword(){
            this.weaponName="Sword";
            this.weaponDescription="Great Sword";
            this.weaponValue=25;
        }

    @Override
    public int getWeaponValue() {
        return this.weaponValue;
    }
    @Override
    public String getDescription(){
        return this.weaponDescription;
    }
    @Override
    public String getName(){
        return this.weaponName;
    }
}
