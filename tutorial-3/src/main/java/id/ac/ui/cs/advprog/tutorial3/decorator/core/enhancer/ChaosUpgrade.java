package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;

    public ChaosUpgrade(Weapon weapon) {
        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int buff=new Random().nextInt(6)+50;
        this.weaponValue=buff+this.weapon.getWeaponValue();
        return this.weaponValue;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        this.weaponDescription="Chaos "+this.weapon.getDescription();
        return this.weaponDescription;
    }
}
