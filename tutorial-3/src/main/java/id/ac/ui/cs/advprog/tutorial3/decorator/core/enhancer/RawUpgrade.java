package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;

    public RawUpgrade(Weapon weapon) {
        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int buff=new Random().nextInt(6)+5;
        this.weaponValue=buff+this.weapon.getWeaponValue();
        return this.weaponValue;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        this.weaponDescription="Raw "+this.weapon.getDescription();
        return this.weaponDescription;
    }
}
