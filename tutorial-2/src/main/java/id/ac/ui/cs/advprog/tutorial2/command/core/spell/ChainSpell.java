package id.ac.ui.cs.advprog.tutorial2.command.core.spell;
import java.util.*;
public class ChainSpell implements Spell {
    // TODO: Complete Me
    protected ArrayList <Spell> spells;
    public ChainSpell(ArrayList <Spell> spells){
        this.spells=spells;
    }
    @Override
    public String spellName() {
        return "ChainSpell";
    }
    @Override
    public void undo(){
        for (int i=spells.size()-1;i >= 0;i--){
            spells.get(i).undo();
        }
    }

    public void cast(){
        for (Spell spell: spells){
            spell.cast();
        }
    }
}
