package id.ac.ui.cs.advprog.tutorial4.abstractfactory.armory.skill;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ShiningForce;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.Skill;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShiningForceTest {

    Skill shiningForce;

    @BeforeEach
    public void setUp(){
        shiningForce = new ShiningForce();
    }

    @Test
    public void testToString(){
        // TODO create test
        assertEquals("Shining Force",shiningForce.getName());
    }

    @Test
    public void testDescription(){
        // TODO create test
        assertEquals("Light magic skills. Greatly damage dark elemental enemies. Ineffective against light-type enemies.",shiningForce.getDescription());
    }
}
