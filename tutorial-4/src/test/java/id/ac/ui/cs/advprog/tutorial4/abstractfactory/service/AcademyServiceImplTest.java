package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;
import java.util.*;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyService academyService;

    // TODO create tests
    public void setUp(){
        academyService=new AcademyServiceImpl(academyRepository);
    }
    public void testAcademiesList(){
        Map <String,KnightAcademy> knightAcademies=new HashMap<>();
        knightAcademies.put("Lordran",new LordranAcademy());
        knightAcademies.put("Drangleic",new DrangleicAcademy());
        assertEquals(knightAcademies,academyRepository.getKnightAcademies());
    }
    public void testKnightCreation(){
        academyService.produceKnight("Lordran","majestic");
        assertEquals(academyRepository.getKnightAcademyByName("Lordran").produceKnight("majestic"),academyService.getKnight());
    }


}
