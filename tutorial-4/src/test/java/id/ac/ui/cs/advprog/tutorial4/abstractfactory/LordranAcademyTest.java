package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class LordranAcademyTest {
    KnightAcademy lordranAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        lordranAcademy=new LordranAcademy();
        majesticKnight=lordranAcademy.produceKnight("majestic");
        metalClusterKnight=lordranAcademy.produceKnight("metal cluster");
        syntheticKnight=lordranAcademy.produceKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight instanceof  MajesticKnight);
        assertTrue(metalClusterKnight instanceof  MetalClusterKnight);
        assertTrue(syntheticKnight instanceof  SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        assertEquals("Lordran Majestic Knight",majesticKnight.getName());
        assertEquals("Lordran Metal Cluster Knight",metalClusterKnight.getName());
        assertEquals("Lordran Synthetic Knight",syntheticKnight.getName());
    }
    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertEquals("Thousand Jacker",majesticKnight.getWeapon().getName());
        assertEquals("Metal Armor",majesticKnight.getArmor().getName());
        assertEquals("Metal Armor",metalClusterKnight.getArmor().getName());
        assertEquals("Thousand Years of Pain",metalClusterKnight.getSkill().getName());
        assertEquals("Thousand Jacker",syntheticKnight.getWeapon().getName());
        assertEquals("Thousand Years of Pain",syntheticKnight.getSkill().getName());
    }
    @Test
    public void testName(){
        assertEquals("Lordran",lordranAcademy.getName());
    }


}
