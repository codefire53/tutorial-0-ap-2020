
package id.ac.ui.cs.advprog.tutorial4.singleton.service;
import org.junit.jupiter.api.Test;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    // TODO create tests

    @Test
    public void testWish(){
        HolyGrail holyGrr=new HolyGrail();
        holyGrr.makeAWish("God...give me exusiasi/firewatch in the next roll please!");
        assertEquals("God...give me exusiasi/firewatch in the next roll please!",holyGrr.getHolyWish().getWish());
    }
    @Test
    public void wishInstanceNotNull(){
        HolyGrail holyGrr=new HolyGrail();
        assertNotNull(holyGrr.getHolyWish());
    }

    @Test
    public void wishShouldBeSingleton(){
        HolyGrail holyGrr=new HolyGrail();
        HolyWish holywish=HolyWish.getInstance();
        assertEquals(holywish,holyGrr.getHolyWish());
    }
}
