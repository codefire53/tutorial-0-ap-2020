package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        drangleicAcademy=new DrangleicAcademy();
        majesticKnight=drangleicAcademy.produceKnight("majestic");
        metalClusterKnight=drangleicAcademy.produceKnight("metal cluster");
        syntheticKnight=drangleicAcademy.produceKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight instanceof  MajesticKnight);
        assertTrue(metalClusterKnight instanceof  MetalClusterKnight);
        assertTrue(syntheticKnight instanceof  SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        assertEquals("Drangleic Majestic Knight",majesticKnight.getName());
        assertEquals("Drangleic Metal Cluster Knight",metalClusterKnight.getName());
        assertEquals("Drangleic Synthetic Knight",syntheticKnight.getName());
    }
    @Test
    public void checkKnightDescriptions() {
        // TODO create test\
        assertEquals("Shining Buster",majesticKnight.getWeapon().getName());
        assertEquals("Shining Armor",majesticKnight.getArmor().getName());
        assertEquals("Shining Armor",metalClusterKnight.getArmor().getName());
        assertEquals("Shining Force",metalClusterKnight.getSkill().getName());
        assertEquals("Shining Buster",syntheticKnight.getWeapon().getName());
        assertEquals("Shining Force",syntheticKnight.getSkill().getName());
    }
    @Test
    public void testName(){
        assertEquals("Drangleic",drangleicAcademy.getName());
    }
}
