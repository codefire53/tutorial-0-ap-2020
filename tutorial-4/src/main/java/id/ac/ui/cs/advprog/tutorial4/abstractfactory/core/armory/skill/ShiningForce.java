package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill;

public class ShiningForce implements Skill {

    @Override
    public String getName() {
        // TODO fix me
        return "Shining Force";
    }

    @Override
    public String getDescription() {
        // TODO fix me
        return "Light magic skills. Greatly damage dark elemental enemies. Ineffective against light-type enemies.";
    }
}
